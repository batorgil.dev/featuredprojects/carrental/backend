package settings

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/utils"
)

type CouponController struct {
	common.Controller
}

func (co CouponController) Register(router fiber.Router) {
	router.Post("page", co.Pagination).Name("page")
	router.Post("create", co.Create).Name("create")
	router.Delete("delete/:id", co.Delete).Name("delete")
	router.Put("update/:id", co.Update).Name("update")
}

// controller start

type (
	CouponPageFilterInput struct {
		database.PaginationInput
		Code      *string   `json:"code"`
		IsUsed    *string   `json:"is_used"`
		CreatedAt []*string `json:"created_at"`
	}
)

// @Summary	Coupon pagination
// @Tags		[Staff] [Settings] Coupon
// @Param		filter	body		CouponPageFilterInput	false	"Filter"
// @Success	200		{object}	common.BaseResponse{body=common.PaginationResponse{items=[]databases.Coupon}}
// @Router		/staff/settings/coupon/page [post]
func (co CouponController) Pagination(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params CouponPageFilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.Coupon{}),
	}

	orm = orm.
		Bool("is_used", params.IsUsed).
		Like("code", params.Code).
		BetweenDate("created_at", params.CreatedAt)

	result := common.PaginationResponse{
		Total: orm.Total(),
	}

	items := []databases.Coupon{}
	if err := orm.Scopes(database.Paginate(&params.PaginationInput)).
		Find(&items).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	result.Items = items
	return co.SetBody(result)
}

type CouponCreateInput struct {
	Code   string  `json:"code"`
	Price  float64 `json:"price"`
	IsUsed bool    `json:"is_used"`
}

// @Summary	Coupon create
// @Tags		[Staff] [Settings] Coupon
// @Param		input	body		CouponCreateInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/staff/settings/coupon/create [post]
func (co CouponController) Create(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params CouponCreateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	if err := database.DB.Create(&databases.Coupon{
		Code:   params.Code,
		Price:  params.Price,
		IsUsed: params.IsUsed,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

type CouponUpdateInput struct {
	Code   string  `json:"code"`
	Price  float64 `json:"price"`
	IsUsed bool    `json:"is_used"`
}

// @Summary	Coupon edit
// @Tags		[Staff] [Settings] Coupon
// @Param		id		path		int					true	"Coupon ID"
// @Param		input	body		CouponUpdateInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/staff/settings/coupon/update/:id [put]
func (co CouponController) Update(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params CouponUpdateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	ID := c.Params("id")
	if err := database.DB.Model(&databases.Coupon{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Updates(map[string]interface{}{
		"code":    params.Code,
		"price":   params.Price,
		"is_used": params.IsUsed,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

// @Summary	Coupon delete
// @Tags		[Staff] [Settings] Coupon
// @Param		id	path		int	true	"Coupon ID"
// @Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/staff/settings/coupon/delete/:id [delete]
func (co CouponController) Delete(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	if err := database.DB.Delete(&databases.Coupon{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}
