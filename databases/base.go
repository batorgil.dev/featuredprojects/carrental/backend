package databases

import (
	"encoding/json"
	"time"

	"gorm.io/gorm"
)

type JSON json.RawMessage

type (
	// Base struct
	Base struct {
		ID        uint           `gorm:"primary_key;autoIncrement:true" json:"id"`
		CreatedAt time.Time      `gorm:"autoCreateTime" json:"created_at"`
		UpdatedAt time.Time      `gorm:"autoUpdateTime" json:"updated_at"`
		DeletedAt gorm.DeletedAt `gorm:"index" json:"-"`
	}
)
