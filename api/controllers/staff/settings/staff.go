package settings

import (
	"errors"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/constants"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/utils"
)

type StaffController struct {
	common.Controller
}

func (co StaffController) Register(router fiber.Router) {
	router.Post("page", co.Pagination).Name("page")
	router.Post("create", co.Create).Name("create")
	router.Delete("delete/:id", co.Delete).Name("delete")
	router.Put("update/:id", co.Update).Name("update")
	router.Post("change_password/:id", co.ChangePassword).Name("change_password")
}

// controller start

type (
	StaffPageFilterInput struct {
		database.PaginationInput
		FirstName *string             `json:"first_name"`
		LastName  *string             `json:"last_name"`
		Email     *string             `json:"email"`
		Position  *string             `json:"position"`
		Bio       *string             `json:"bio"`
		IsActive  *string             `json:"is_active"`
		Role      *constants.UserRole `json:"role"`
		CreatedAt []*string           `json:"created_at"`
	}
)

// @Summary	Staff pagination
// @Tags		[Staff] Staff
// @Param		filter	body		StaffPageFilterInput	false	"Filter"
// @Success	200		{object}	common.BaseResponse{body=common.PaginationResponse{items=[]databases.Staff}}
// @Router		/staff/staff/page [post]
func (co StaffController) Pagination(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params StaffPageFilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.Staff{}),
	}

	orm = orm.
		Like("first_name", params.FirstName).
		Like("last_name", params.LastName).
		Like("email", params.Email).
		Like("position", params.Position).
		Like("bio", params.Bio).
		Equal("is_active", params.IsActive).
		Equal("role", params.Role).
		BetweenDate("created_at", params.CreatedAt)

	result := common.PaginationResponse{
		Total: orm.Total(),
	}

	items := []databases.Staff{}
	if err := orm.Scopes(database.Paginate(&params.PaginationInput)).
		Find(&items).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	result.Items = items
	return co.SetBody(result)
}

type StaffCreateInput struct {
	FirstName  string             `json:"first_name"`
	LastName   string             `json:"last_name"`
	AvatarPath string             `json:"avatar_path"`
	Email      string             `json:"email"`
	Position   string             `json:"position"`
	Bio        string             `json:"bio"`
	Password   string             `json:"password"`
	IsActive   bool               `json:"is_active"`
	Role       constants.UserRole `json:"role"`
}

// @Summary	Staff create
// @Tags		[Staff] Staff
// @Param		input	body		StaffCreateInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/staff/staff/create [post]
func (co StaffController) Create(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params StaffCreateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	hashPwd, err := utils.GenerateHash(params.Password)
	if err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, errors.New("Failed to generate hash password :"+err.Error()))
	}

	if err := database.DB.Create(&databases.Staff{
		FirstName:  params.FirstName,
		LastName:   params.LastName,
		AvatarPath: params.AvatarPath,
		Email:      params.Email,
		Position:   params.Position,
		Bio:        params.Bio,
		Password:   hashPwd,
		IsActive:   params.IsActive,
		Role:       params.Role,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

type StaffUpdateInput struct {
	FirstName  string             `json:"first_name"`
	LastName   string             `json:"last_name"`
	AvatarPath string             `json:"avatar_path"`
	Email      string             `json:"email"`
	Position   string             `json:"position"`
	Bio        string             `json:"bio"`
	IsActive   bool               `json:"is_active"`
	Role       constants.UserRole `json:"role"`
}

// @Summary	Staff edit
// @Tags		[Staff] Staff
// @Param		id		path		int					true	"Staff ID"
// @Param		input	body		StaffUpdateInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/staff/staff/update/:id [put]
func (co StaffController) Update(c *fiber.Ctx) error {
	tx := database.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
			co.RespondPanic(c, r)
		} else if co.Res.StatusCode != fiber.StatusOK {
			tx.Rollback()
			co.GetBody(c)
		} else {
			tx.Commit()
			co.GetBody(c)
		}
	}()

	var params StaffUpdateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	ID := c.Params("id")

	user := databases.Staff{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}
	if err := tx.Model(&user).Updates(map[string]interface{}{
		"first_name":  params.FirstName,
		"last_name":   params.LastName,
		"avatar_path": params.AvatarPath,
		"email":       params.Email,
		"position":    params.Position,
		"bio":         params.Bio,
		"is_active":   params.IsActive,
		"role":        params.Role,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

// @Summary	Staff delete
// @Tags		[Staff] Staff
// @Param		id	path		int	true	"Staff ID"
// @Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/staff/staff/delete/:id [delete]
func (co StaffController) Delete(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	if err := database.DB.Delete(&databases.Staff{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

type ChangePasswordInput struct {
	Password string `json:"password"`
}

// @Summary	Staff нууц үг өөрчлөх
// @Tags		[Staff] Staff
// @Param		id		path		int					true	"Staff ID"
// @Param		input	body		ChangePasswordInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/user/change_password/:id [post]
func (co StaffController) ChangePassword(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params ChangePasswordInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	hashPwd, err := utils.GenerateHash(params.Password)
	if err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, errors.New("Failed to generate hash password :"+err.Error()))
	}

	ID := c.Params("id")
	if err := database.DB.Model(&databases.Staff{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Updates(map[string]interface{}{
		"password": hashPwd,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}
