---
inject: true
to: api/controllers/<%= (function(){ let finalPath = path.split('/'); finalPath.pop(); finalPath.push('base'); return finalPath.join('/') })() %>.go
skip_if: "gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/middlewares"
after: "import \\("
---
  <% if (middleware) { %>"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/middlewares"<%}%>