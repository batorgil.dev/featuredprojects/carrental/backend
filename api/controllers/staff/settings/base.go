package settings

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/middlewares"
)

func Register(bc common.Controller, router fiber.Router) {
	// new routers
	// new controllers
  ConfigController{Controller: bc}.Register(router.Group("config" , middlewares.Auth).Name("config."))
  ContactController{Controller: bc}.Register(router.Group("contact" , middlewares.Auth).Name("contact."))
  EmailTemplateController{Controller: bc}.Register(router.Group("email_template" , middlewares.Auth).Name("email_template."))
  CouponController{Controller: bc}.Register(router.Group("coupon" , middlewares.Auth).Name("coupon."))
  LocationController{Controller: bc}.Register(router.Group("location" , middlewares.Auth).Name("location."))
  InsuranceCompanyController{Controller: bc}.Register(router.Group("insurance_company" , middlewares.Auth).Name("insurance_company."))
	StaffController{Controller: bc}.Register(router.Group("staff", middlewares.Auth).Name("staff."))
	CarTypeController{Controller: bc}.Register(router.Group("car_type", middlewares.Auth).Name("car_type."))
}
