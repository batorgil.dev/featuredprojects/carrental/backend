package settings

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/integrations/storage"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/utils"
)

type ContactController struct {
	common.Controller
}

func (co ContactController) Register(router fiber.Router) {
	router.Post("page", co.Pagination).Name("page")
	router.Post("create", co.Create).Name("create")
	router.Delete("delete/:id", co.Delete).Name("delete")
	router.Put("update/:id", co.Update).Name("update")
}

// controller start

type (
	ContactPageFilterInput struct {
		database.PaginationInput
		Name      *string   `json:"name"`
		Phone     *string   `json:"phone"`
		CreatedAt []*string `json:"created_at"`
	}
)

// @Summary	Contact pagination
// @Tags		[Staff] [Settings] Contact
// @Param		filter	body		ContactPageFilterInput	false	"Filter"
// @Success	200		{object}	common.BaseResponse{body=common.PaginationResponse{items=[]databases.Contact}}
// @Router		/staff/settings/contact/page [post]
func (co ContactController) Pagination(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params ContactPageFilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.Contact{}),
	}

	orm = orm.
		Like("name", params.Name).
		Like("phone", params.Phone).
		BetweenDate("created_at", params.CreatedAt)

	result := common.PaginationResponse{
		Total: orm.Total(),
	}

	items := []databases.Contact{}
	if err := orm.Scopes(database.Paginate(&params.PaginationInput)).
		Find(&items).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	result.Items = items
	return co.SetBody(result)
}

type ContactCreateInput struct {
	IconPath string `json:"icon_path"`
	Name     string `json:"name"`
	Phone    string `json:"phone"`
}

// @Summary	Contact create
// @Tags		[Staff] [Settings] Contact
// @Param		input	body		ContactCreateInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/staff/settings/contact/create [post]
func (co ContactController) Create(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params ContactCreateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	if err := database.DB.Create(&databases.Contact{
		IconPath: params.IconPath,
		Name:     params.Name,
		Phone:    params.Phone,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

type ContactUpdateInput struct {
	IconPath string `json:"icon_path"`
	Name     string `json:"name"`
	Phone    string `json:"phone"`
}

// @Summary	Contact edit
// @Tags		[Staff] [Settings] Contact
// @Param		id		path		int					true	"Contact ID"
// @Param		input	body		ContactUpdateInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/staff/settings/contact/update/:id [put]
func (co ContactController) Update(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params ContactUpdateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	ID := c.Params("id")
	if err := database.DB.Model(&databases.Contact{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Updates(map[string]interface{}{
		"icon_path": params.IconPath,
		"name":      params.Name,
		"phone":     params.Phone,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

// @Summary	Contact delete
// @Tags		[Staff] [Settings] Contact
// @Param		id	path		int	true	"Contact ID"
// @Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/staff/settings/contact/delete/:id [delete]
func (co ContactController) Delete(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	contact := databases.Contact{}
	if err := database.DB.First(&contact, ID).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	if err := database.DB.Unscoped().Delete(&databases.Contact{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	st := storage.Connect()
	st.RemoveFile(contact.IconPath)

	return co.SetBody(common.SuccessResponse{Success: true})
}
