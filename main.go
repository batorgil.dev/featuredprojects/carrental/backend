package main

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"github.com/jasonlvhit/gocron"
	"github.com/spf13/viper"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/utils"
)

//	@title			CarrentalAPI v1
//	@version		1.0
//	@description	CarrentalAPI v1.
//	@termsOfService	https://carrental.batorgil.dev/

//	@contact.name	API Support
//	@contact.url	https://www.facebook.com/bat.orgil.kok/
//	@contact.email	hello@batorgil.dev

//	@license.name	Apache 2.0
//	@license.url	http://www.apache.org/licenses/LICENSE-2.0.html

// @BasePath	/api/v1
// @schemes	http https
// @accept		json
// @produce	json
func main() {
	utils.LoadConfig()
	utils.LoadLogger()
	database.LoadDatabase()

	app := fiber.New(
		fiber.Config{
			AppName:               viper.GetString("APP_NAME"),
			Prefork:               viper.GetBool("APP_PREFORK"),
			ErrorHandler:          utils.FiberErrorHandler,
			DisableStartupMessage: false,
			StrictRouting:         true,
			CaseSensitive:         false,
			// EnablePrintRoutes:     true,
		},
	)

	go func() {
		fmt.Println("Cron jobs inited")
		// gocron.Every(1).Day().At("02:00:00").Do(services.SomeTask)
		gocron.Start()
	}()

	api.Register(app)
	app.Listen("0.0.0.0:" + viper.GetString("APP_PORT"))
}
