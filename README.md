# How to start project

1. Install Golang on your system https://go.dev/doc/install
2. I use Docker for local deployments. Install docker and run following commands

```
# For storage used S3 compatible storage called Minio.
# Small code change you can use AWS S3 as storage.
docker run -d --restart always -p 9000:9000 -e MINIO_ROOT_USER=admin -e MINIO_ROOT_PASSWORD=admin123 -v ~/carrental-data:/data --name carrental-storage minio/minio:RELEASE.2021-04-06T23-11-00Z-24-g965afa971 minio server /data
# For database used Postgresql
docker run -d --restart always -p 5000:5432 --name carrental-db -e POSTGRES_DB=carrental -e POSTGRES_USER=app -e POSTGRES_PASSWORD=app1234 -d postgres
```

After these steps you are ready to start working on project. run following commands in your codebase.

```
# Install nessesary packages
go mod tidy
# Before this step please configure enviroment variables in .env file.
go run main.go
```

**Once server is up and running you can navigate to swagger by following url:**
[swagger] - http://localhost:8000/swagger/index.html

# Project structure

Project has following root folders.\
├── \_templates - hygen code generation tool to generate controller and router.\
├── api - API related resources like controllers, middlewares, swagger docs.\
├── constants - stores system constants.\
├── databases - stores system database models.\
├── files - stores system used files like mail template, if server serves static file then it will be stored in here.\
├── integrations - third-party system integration like database, storage or email.\
├── services - controller shared functions and if system has any kinds of jobs. \(cronjob) it will placed here.\
└── utils - stored shared function in system.

# Mean libraries

**Gorm** - Used as database connector tool.\
**Fiber** - Golang http server.\
**MinioSKD** - Used for storing files in S3 compatible API which means it can be used on AWS s3, Wasabi, Minio server or any S3 compatible API providers.

# Tech stack

- Postgres
- Minio server
- Golang
