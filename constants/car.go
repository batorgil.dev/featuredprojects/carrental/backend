package constants

type Steering string

const (
	SteeringManual   Steering = "manual"
	SteeringAuto     Steering = "auto"
	SteeringCombined Steering = "combined"
)

type CarStatus string

const (
	CarStatusAvailable CarStatus = "available"
	CarStatusInService CarStatus = "in_service"
	CarStatusOnRent    CarStatus = "on_rent"
)

type DamageType string

const (
	DamageTypeFront DamageType = "front"
	DamageTypeBack  DamageType = "back"
)

type DamageLocation string

const (
	DamageLocationLeft  DamageLocation = "left"
	DamageLocationRight DamageLocation = "right"
)
