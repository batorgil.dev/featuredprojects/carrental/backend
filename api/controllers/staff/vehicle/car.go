package vehicle

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"github.com/lib/pq"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/constants"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/utils"
)

type CarController struct {
	common.Controller
}

func (co CarController) Register(router fiber.Router) {
	router.Post("page", co.Pagination).Name("page")
	router.Get("get/:id", co.Get).Name("get")
	router.Post("create", co.Create).Name("create")
	router.Delete("delete/:id", co.Delete).Name("delete")
	router.Put("update/:id", co.Update).Name("update")
	router.Put("update_status/:id", co.UpdateStatus).Name("update_status")
}

// controller start

type (
	CarPageFilterInput struct {
		database.PaginationInput
		Name                 *string              `json:"name"`
		VinNo                *string              `json:"vin_no"`
		LicenceNo            *string              `json:"licence_no"`
		Color                *string              `json:"color"`
		Description          *string              `json:"description"`
		Steering             *constants.Steering  `json:"steering"`
		IsFeatured           *string              `json:"is_featured"`
		IsPopular            *string              `json:"is_popular"`
		Status               *constants.CarStatus `json:"status"`
		InsuranceID          *uint                `json:"insurance_id"`
		CarTypeID            *uint                `json:"car_type_id"`
		CarTypeName          *string              `json:"car_type_name"`
		InsuranceCompanyName *string              `json:"insurance_company_name"`
		CreatedAt            []*string            `json:"created_at"`
	}
	CarItem struct {
		databases.Car
		CarTypeName          string `gorm:"car_type_name" json:"car_type_name"`
		InsuranceCompanyName string `gorm:"insurance_company_name" json:"insurance_company_name"`
	}
)

// @Summary	Car pagination
// @Tags		[Staff] [Vehicle] Car
// @Param		filter	body		CarPageFilterInput	false	"Filter"
// @Success	200		{object}	common.BaseResponse{body=common.PaginationResponse{items=[]CarItem}}
// @Router		/staff/vehicle/car/page [post]
func (co CarController) Pagination(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params CarPageFilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.Car{}).
			Joins("left join car_types on car_types.id = cars.car_type_id").
			Joins("left join insurance_companies on insurance_companies.id = cars.insurance_id"),
	}

	orm = orm.
		Like("cars.name", params.Name).
		Like("cars.vin_no", params.VinNo).
		Like("cars.licence_no", params.LicenceNo).
		Like("cars.color", params.Color).
		Like("cars.description", params.Description).
		Like("car_types.name", params.CarTypeName).
		Like("insurance_companies.name", params.InsuranceCompanyName).
		Equal("cars.steering", params.Steering).
		Bool("cars.is_featured", params.IsFeatured).
		Bool("cars.is_popular", params.IsPopular).
		Equal("cars.status", params.Status).
		Equal("cars.insurance_id", params.InsuranceID).
		Equal("cars.car_type_id", params.CarTypeID).
		BetweenDate("created_at", params.CreatedAt)

	result := common.PaginationResponse{
		Total: orm.Total(),
	}

	items := []CarItem{}
	if err := orm.Scopes(database.Paginate(&params.PaginationInput)).
		Select("cars.*, car_types.name as car_type_name, insurance_companies.name as insurance_company_name").
		Find(&items).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	result.Items = items
	return co.SetBody(result)
}

// @Summary	Car detail
// @Tags		[Staff] [Vehicle] Car
// @Param		id	path		int	true	"Car ID"
// @Success	200	{object}	common.BaseResponse{body=databases.Car}
// @Router		/staff/vehicle/car/get/:id [get]
func (co CarController) Get(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	var instance databases.Car
	if err := database.DB.Model(&databases.Car{}).
		Preload("Insurance").
		Preload("CarType").
		First(&instance, ID).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}
	return co.SetBody(instance)
}

type CarCreateInput struct {
	Name        string             `json:"name"`
	VinNo       string             `json:"vin_no"`
	LicenceNo   string             `json:"licence_no"`
	Color       string             `json:"color"`
	Casoline    int64              `json:"casoline"`
	Description string             `json:"description"`
	Steering    constants.Steering `json:"steering"`
	Capacity    int64              `json:"capacity"`
	Baggage     int64              `json:"baggage"`
	ImagePaths  []string           `json:"image_paths"`
	IsFeatured  bool               `json:"is_featured"`
	IsPopular   bool               `json:"is_popular"`
	Price       float64            `json:"price"`
	InsuranceID uint               `json:"insurance_id"`
	CarTypeID   uint               `json:"car_type_id"`
}

// @Summary	Car create
// @Tags		[Staff] [Vehicle] Car
// @Param		input	body		CarCreateInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/staff/vehicle/car/create [post]
func (co CarController) Create(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params CarCreateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	if err := database.DB.Create(&databases.Car{
		Name:        params.Name,
		VinNo:       params.VinNo,
		LicenceNo:   params.LicenceNo,
		Color:       params.Color,
		Casoline:    params.Casoline,
		Description: params.Description,
		Steering:    params.Steering,
		Capacity:    params.Capacity,
		Baggage:     params.Baggage,
		ImagePaths:  pq.StringArray(params.ImagePaths),
		IsFeatured:  params.IsFeatured,
		IsPopular:   params.IsPopular,
		Price:       params.Price,
		InsuranceID: params.InsuranceID,
		CarTypeID:   params.CarTypeID,
		Status:      constants.CarStatusAvailable,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

type CarUpdateInput struct {
	Name        string             `json:"name"`
	VinNo       string             `json:"vin_no"`
	LicenceNo   string             `json:"licence_no"`
	Color       string             `json:"color"`
	Casoline    int64              `json:"casoline"`
	Description string             `json:"description"`
	Steering    constants.Steering `json:"steering"`
	Capacity    int64              `json:"capacity"`
	Baggage     int64              `json:"baggage"`
	ImagePaths  []string           `json:"image_paths"`
	IsFeatured  bool               `json:"is_featured"`
	IsPopular   bool               `json:"is_popular"`
	Price       float64            `json:"price"`
	InsuranceID uint               `json:"insurance_id"`
	CarTypeID   uint               `json:"car_type_id"`
}

// @Summary	Car edit
// @Tags		[Staff] [Vehicle] Car
// @Param		id		path		int					true	"Car ID"
// @Param		input	body		CarUpdateInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/staff/vehicle/car/update/:id [put]
func (co CarController) Update(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params CarUpdateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	ID := c.Params("id")

	if err := database.DB.Model(&databases.Car{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Updates(map[string]interface{}{
		"name":         params.Name,
		"vin_no":       params.VinNo,
		"licence_no":   params.LicenceNo,
		"color":        params.Color,
		"casoline":     params.Casoline,
		"description":  params.Description,
		"steering":     params.Steering,
		"capacity":     params.Capacity,
		"baggage":      params.Baggage,
		"image_paths":  pq.StringArray(params.ImagePaths),
		"is_featured":  params.IsFeatured,
		"is_popular":   params.IsPopular,
		"price":        params.Price,
		"insurance_id": params.InsuranceID,
		"car_type_id":  params.CarTypeID,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

type CarUpdateStatusInput struct {
	Status constants.CarStatus `json:"status"`
}

// @Summary	Car status update
// @Tags		[Staff] [Vehicle] Car
// @Param		id		path		int					true	"Car ID"
// @Param		input	body		CarUpdateStatusInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/staff/vehicle/car/update_status/:id [put]
func (co CarController) UpdateStatus(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params CarUpdateStatusInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	ID := c.Params("id")
	if err := database.DB.Where("status = ? and id = ?", constants.CarStatusOnRent, ID).First(&databases.Car{}).Error; err == nil {
		return co.SetError(c, fiber.StatusInternalServerError, fmt.Errorf("you cannot update on rent car's status"))
	}

	if err := database.DB.Model(&databases.Car{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Updates(map[string]interface{}{
		"status": params.Status,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

// @Summary	Car delete
// @Tags		[Staff] [Vehicle] Car
// @Param		id	path		int	true	"Car ID"
// @Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/staff/vehicle/car/delete/:id [delete]
func (co CarController) Delete(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")

	if err := database.DB.Where("status = ? and id = ?", constants.CarStatusOnRent, ID).First(&databases.Car{}).Error; err == nil {
		return co.SetError(c, fiber.StatusInternalServerError, fmt.Errorf("you cannot delete car on rent status"))
	}

	if err := database.DB.Delete(&databases.Car{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}
