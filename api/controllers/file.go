package controllers

import (
	"context"
	"errors"
	"net/http"
	"path/filepath"

	"github.com/gofiber/fiber/v2"
	"github.com/gofrs/uuid"
	"github.com/minio/minio-go/v7"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/constants"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/integrations/storage"
)

type FileController struct {
	common.Controller
}

func (co FileController) Register(router fiber.Router) {
	router.Post("/upload", co.Upload).Name("upload")
	router.Post("/multi/upload", co.MultiUpload).Name("multi.upload")
}

type UploadResponse struct {
	Name string `json:"name"`
	Path string `json:"path"`
}

// @Summary	Upload single file
// @Tags		[User] File
// @Param		name	formData	string	true	"Name"
// @Param		file	formData	file	true	"File"
// @Success	200		{object}	common.BaseResponse{body=UploadResponse}
// @Router		/user/file/upload [post]
func (co FileController) Upload(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	storageClient := storage.Connect()

	u2, err := uuid.NewV4()
	if err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	file, _ := c.FormFile("file")

	bucketName := c.FormValue("bucket_name", constants.DefaultBucketName)

	if err := storageClient.CreatePublicBucket(bucketName); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	types := file.Header["Content-Type"]
	fileName := u2.String() + filepath.Ext(file.Filename)
	path := ""

	openFile, errorfileOpen := file.Open()
	if errorfileOpen != nil {
		return co.SetError(c, fiber.StatusBadRequest, errors.New("file open failed: "+errorfileOpen.Error()))
	}

	uploadInfo, errUploadInfo := storageClient.PutObject(context.Background(), bucketName, fileName, openFile, file.Size, minio.PutObjectOptions{ContentType: types[0]})
	if errUploadInfo != nil {
		return co.SetError(c, fiber.StatusBadRequest, errors.New("file upload to minio failed:. ["+errUploadInfo.Error()+"]"))
	}

	path = uploadInfo.Bucket + "/" + uploadInfo.Key

	return co.SetBody(UploadResponse{
		Name: fileName,
		Path: path,
	})
}

// @Summary	Upload multiple file
// @Tags		[User] File
// @Param		names	formData	[]string	true	"Names"
// @Param		files	formData	file		true	"Files"
// @Success	200		{object}	common.BaseResponse{body=UploadResponse}
// @Router		/user/file/multi/upload [post]
func (co FileController) MultiUpload(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	storageClient := storage.Connect()

	formdata, err := c.MultipartForm()
	if err != nil {
		return co.SetError(c, http.StatusBadRequest, err)
	}

	names := formdata.Value["names"]
	files := formdata.File["files"]

	if len(files) == 0 {
		return co.SetError(c, fiber.StatusBadRequest, errors.New("empty list provided"))
	}
	if len(files) != len(names) {
		return co.SetError(c, fiber.StatusBadRequest, errors.New("files and names are not matching"))
	}

	bucketName := c.FormValue("bucket_name", constants.DefaultBucketName)

	if bucketName != constants.DefaultBucketName {
		if err := storageClient.CreatePublicBucket(bucketName); err != nil {
			return co.SetError(c, fiber.StatusBadRequest, err)
		}
	}

	var allFiles []UploadResponse
	for _, file := range files {
		types := file.Header["Content-Type"]
		u2, err := uuid.NewV4()
		if err != nil {
			return co.SetError(c, fiber.StatusBadRequest, errors.New("uuid create failed: "+err.Error()))
		}
		fileName := u2.String() + filepath.Ext(file.Filename)
		path := ""

		openFile, errorfileOpen := file.Open()
		if errorfileOpen != nil {
			return co.SetError(c, fiber.StatusBadRequest, errors.New("file open failed: "+errorfileOpen.Error()))
		}

		uploadInfo, errUploadInfo := storageClient.PutObject(context.Background(), bucketName, fileName, openFile, file.Size, minio.PutObjectOptions{ContentType: types[0]})
		if errUploadInfo != nil {
			return co.SetError(c, fiber.StatusBadRequest, errors.New("file upload to minio failed:. ["+errUploadInfo.Error()+"]"))
		}

		path = uploadInfo.Bucket + "/" + uploadInfo.Key
		allFiles = append(allFiles, UploadResponse{
			Name: fileName,
			Path: path,
		})
	}

	return co.SetBody(allFiles)
}
