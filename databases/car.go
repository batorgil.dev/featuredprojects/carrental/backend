package databases

import (
	"time"

	"github.com/lib/pq"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/constants"
)

type (
	Car struct {
		Base
		Name        string              `gorm:"column:name" json:"name"`
		VinNo       string              `gorm:"column:vin_no" json:"vin_no"`
		LicenceNo   string              `gorm:"column:licence_no" json:"licence_no"`
		Color       string              `gorm:"column:color" json:"color"`
		Casoline    int64               `gorm:"column:casoline" json:"casoline"`
		Description string              `gorm:"column:description" json:"description"`
		Steering    constants.Steering  `gorm:"column:steering" json:"steering"`
		Capacity    int64               `gorm:"column:capacity" json:"capacity"`
		Baggage     int64               `gorm:"column:baggage" json:"baggage"`
		ImagePaths  pq.StringArray      `gorm:"type:text[];column:image_paths" json:"image_paths" swaggertype:"array,string"`
		IsFeatured  bool                `gorm:"column:is_featured" json:"is_featured"`
		IsPopular   bool                `gorm:"column:is_popular" json:"is_popular"`
		Price       float64             `gorm:"column:price" json:"price"`
		Status      constants.CarStatus `gorm:"column:status" json:"status"`

		// references
		CarTypeID   uint              `gorm:"column:car_type_id" json:"car_type_id"`
		CarType     *CarType          `gorm:"foreignKey:CarTypeID" json:"car_type,omitempty"`
		InsuranceID uint              `gorm:"column:insurance_id" json:"insurance_id"`
		Insurance   *InsuranceCompany `gorm:"foreignKey:InsuranceID" json:"insurance,omitempty"`
	}

	CarReview struct {
		Base
		CarID         uint      `gorm:"column:car_id" json:"car_id"`
		CustomerID    uint      `gorm:"column:customer_id" json:"customer_id"`
		Customer      *Customer `gorm:"foreignKey:CustomerID" json:"customer,omitempty"`
		ReservationID uint      `gorm:"column:reservation_id" json:"reservation_id"`
		Star          float64   `gorm:"column:star" json:"star"`
		Comment       string    `gorm:"column:comment" json:"comment"`
	}

	CarDamage struct {
		Base
		CarID         uint                     `gorm:"column:car_id" json:"car_id"`
		ReservationID uint                     `gorm:"column:reservation_id" json:"reservation_id"`
		CustomerID    uint                     `gorm:"column:customer_id" json:"customer_id"`
		Type          constants.DamageType     `gorm:"column:type" json:"type"`
		Location      constants.DamageLocation `gorm:"column:location" json:"location"`
		Note          string                   `gorm:"column:note" json:"note"`
	}

	CarTraffic struct {
		Base
		Date          time.Time `gorm:"column:date" json:"date"`
		Note          string    `gorm:"column:note" json:"note"`
		IsPaid        bool      `gorm:"column:is_paid" json:"is_paid"`
		Amount        float64   `gorm:"column:amount" json:"amount"`
		TicketNo      string    `gorm:"column:ticket_no" json:"ticket_no"`
		CarID         uint      `gorm:"column:car_id" json:"car_id"`
		ReservationID uint      `gorm:"column:reservation_id" json:"reservation_id"`
		CustomerID    uint      `gorm:"column:customer_id" json:"customer_id"`
	}

	CarNote struct {
		Base
		CarID uint   `gorm:"column:car_id" json:"car_id"`
		Note  string `gorm:"column:note" json:"note"`
	}

	CarDocument struct {
		Base
		CarID    uint   `gorm:"column:car_id" json:"car_id"`
		Name     string `gorm:"column:name" json:"name"`
		FilePath string `gorm:"column:file_path" json:"file_path"`
	}
)
