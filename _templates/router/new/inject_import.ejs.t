---
inject: true
to: api/controllers/<%= (function(){ let finalPath = name.split('/'); finalPath.pop(); finalPath.push('base'); return finalPath.join('/') })() %>.go
skip_if: "gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/<%= name %>"
after: "import \\("
---
  "gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/<%= name %>"