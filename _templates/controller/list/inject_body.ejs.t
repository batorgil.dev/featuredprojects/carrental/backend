---
inject: true
to: api/controllers/<%= path %>.go
skip_if: "Pagination\\(c *fiber.Ctx\\)"
after: "controller start"
---


type (
	<%= database_model %>FilterInput struct {
		database.SorterInput
		CreatedAt []*string `json:"created_at"`
	}
)

// @Summary	<%= database_model %> list
// @Tags		<%= (function (){let paths = path.split('/'); return paths.map((item, index) => `${index != paths.length - 1 ? `[${item.charAt(0).toUpperCase() + item.slice(1)}]`: `${item.charAt(0).toUpperCase() + item.slice(1)}`}`).join(' ')})() %>
// @Param		filter	body		<%= database_model %>FilterInput	false	"Filter"
// @Success	200		{object}	common.BaseResponse{body=[]databases.<%= database_model %>}
// @Router		/<%= path %>/list [post]
func (co <%= name %>) List(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params <%= database_model %>FilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.<%= database_model %>{}),
	}

	orm = orm.
		BetweenDate("created_at", params.CreatedAt)

	items := []databases.<%= database_model %>{}
	if err := orm.Scopes(database.Sort(&params.Sorter)).
		Find(&items).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	return co.SetBody(items)
}
