package middlewares

import (
	"errors"
	"strings"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/constants"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/utils"
)

const authKeyUser = "auth"

var publicSuperUrls = []string{}

func Auth(c *fiber.Ctx) error {
	headers := c.GetReqHeaders()
	requiredToken, ok := headers["Authorization"]
	if !ok || len(requiredToken) == 0 || len(requiredToken[0]) < 8 {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"message": "Таны нэвтрэх хэрэгтэй",
			"body":    nil,
		})
	}

	claims, err := utils.ExtractJWTString(requiredToken[0][7:])
	if err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"message": "Таны нэвтрэх хугацаа дууссан байна",
			"body":    nil,
		})
	}

	db := database.DB
	userID, ok := claims["id"].(float64)
	if ok {
		var user *databases.Staff
		if err := db.First(&user, uint(userID)).Error; err != nil {
			return errors.New("Хэрэглэгч олдсонгүй")
		}

		if !user.IsActive {
			return errors.New("Та идэвхгүй хэрэглэгч байна")
		}

		c.Locals(authKeyUser, user)

		fullPath := c.OriginalURL()
		if user.Role == constants.UserRoleAdmin {
			return c.Next()
		}

		for _, publicUrl := range publicSuperUrls {
			if strings.Contains(fullPath, publicUrl) {
				return c.Next()
			}
		}
	}

	return c.Status(fiber.StatusForbidden).JSON(fiber.Map{
		"message": "Хандах эрхгүй.",
		"body":    nil,
	})
}

func GetAuth(c *fiber.Ctx) *databases.Staff {
	return c.Locals(authKeyUser).(*databases.Staff)
}
