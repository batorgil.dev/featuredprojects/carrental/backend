package vehicle

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/constants"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/utils"
)

type DamageController struct {
	common.Controller
}

func (co DamageController) Register(router fiber.Router) {
	router.Post("page", co.Pagination).Name("page")
	router.Post("create", co.Create).Name("create")
	router.Delete("delete/:id", co.Delete).Name("delete")
	router.Put("update/:id", co.Update).Name("update")
}

// controller start

type (
	CarDamagePageFilterInput struct {
		database.PaginationInput
		CarID         *uint                     `json:"car_id"`
		ReservationID *uint                     `json:"reservation_id"`
		CustomerID    *uint                     `json:"customer_id"`
		Type          *constants.DamageType     `json:"type"`
		Location      *constants.DamageLocation `json:"location"`
		Note          *string                   `json:"note"`
		CreatedAt     []*string                 `json:"created_at"`
	}
)

//	@Summary	CarDamage pagination
//	@Tags		[Staff] [Vehicle] Damage
//	@Param		filter	body		CarDamagePageFilterInput	false	"Filter"
//	@Success	200		{object}	common.BaseResponse{body=common.PaginationResponse{items=[]databases.CarDamage}}
//	@Router		/staff/vehicle/damage/page [post]
func (co DamageController) Pagination(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params CarDamagePageFilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.CarDamage{}),
	}

	orm = orm.
		Like("note", params.Note).
		Equal("car_id", params.CarID).
		Equal("reservation_id", params.ReservationID).
		Equal("customer_id", params.CustomerID).
		Equal("type", params.Type).
		Equal("location", params.Location).
		BetweenDate("created_at", params.CreatedAt)

	result := common.PaginationResponse{
		Total: orm.Total(),
	}

	items := []databases.CarDamage{}
	if err := orm.Scopes(database.Paginate(&params.PaginationInput)).
		Find(&items).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	result.Items = items
	return co.SetBody(result)
}

type CarDamageCreateInput struct {
	CarID         uint                     `json:"car_id"`
	ReservationID uint                     `json:"reservation_id"`
	CustomerID    uint                     `json:"customer_id"`
	Type          constants.DamageType     `json:"type"`
	Location      constants.DamageLocation `json:"location"`
	Note          string                   `json:"note"`
}

//	@Summary	CarDamage create
//	@Tags		[Staff] [Vehicle] Damage
//	@Param		input	body		CarDamageCreateInput	true	"Input"
//	@Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/staff/vehicle/damage/create [post]
func (co DamageController) Create(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params CarDamageCreateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	if err := database.DB.Create(&databases.CarDamage{
		CarID:         params.CarID,
		ReservationID: params.ReservationID,
		CustomerID:    params.CustomerID,
		Type:          params.Type,
		Location:      params.Location,
		Note:          params.Note,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

type CarDamageUpdateInput struct {
	CarID         uint                     `json:"car_id"`
	ReservationID uint                     `json:"reservation_id"`
	CustomerID    uint                     `json:"customer_id"`
	Type          constants.DamageType     `json:"type"`
	Location      constants.DamageLocation `json:"location"`
	Note          string                   `json:"note"`
}

//	@Summary	CarDamage edit
//	@Tags		[Staff] [Vehicle] Damage
//	@Param		id		path		int						true	"CarDamage ID"
//	@Param		input	body		CarDamageUpdateInput	true	"Input"
//	@Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/staff/vehicle/damage/update/:id [put]
func (co DamageController) Update(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params CarDamageUpdateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	ID := c.Params("id")
	if err := database.DB.Model(&databases.CarDamage{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Updates(map[string]interface{}{
		"car_id":         params.CarID,
		"reservation_id": params.ReservationID,
		"customer_id":    params.CustomerID,
		"type":           params.Type,
		"location":       params.Location,
		"note":           params.Note,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

//	@Summary	CarDamage delete
//	@Tags		[Staff] [Vehicle] Damage
//	@Param		id	path		int	true	"CarDamage ID"
//	@Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/staff/vehicle/damage/delete/:id [delete]
func (co DamageController) Delete(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	if err := database.DB.Unscoped().Delete(&databases.CarDamage{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}
