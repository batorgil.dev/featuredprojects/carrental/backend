---
to: api/controllers/<%= path %>.go
---
package <%= (function (){let paths = path.split('/'); return paths[paths.length -2];})() %>

import (
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/utils"
	"github.com/gofiber/fiber/v2"
)

type <%= name %> struct {
	common.Controller
}

func (co <%= name %>) Register(router fiber.Router) {
	router.Post("page", co.Pagination).Name("page")
	router.Get("get/:id", co.Get).Name("get")
	router.Post("create", co.Create).Name("create")
	router.Delete("delete/:id", co.Delete).Name("delete")
	router.Put("update/:id", co.Update).Name("update")
}

// controller start

type (
	<%= database_model %>PageFilterInput struct {
		database.PaginationInput
		CreatedAt []*string `json:"created_at"`
	}
)

// @Summary	<%= database_model %> pagination
// @Tags		<%= (function (){let paths = path.split('/'); return paths.map((item, index) => `${index != paths.length - 1 ? `[${item.charAt(0).toUpperCase() + item.slice(1)}]`: `${item.charAt(0).toUpperCase() + item.slice(1)}`}`).join(' ')})() %>
// @Param		filter	body		<%= database_model %>PageFilterInput	false	"Filter"
// @Success	200		{object}	common.BaseResponse{body=common.PaginationResponse{items=[]databases.<%= database_model %>}}
// @Router		/<%= path %>/page [post]
func (co <%= name %>) Pagination(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params <%= database_model %>PageFilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.<%= database_model %>{}),
	}

	orm = orm.
		BetweenDate("created_at", params.CreatedAt)

	result := common.PaginationResponse{
		Total: orm.Total(),
	}

	items := []databases.<%= database_model %>{}
	if err := orm.Scopes(database.Paginate(&params.PaginationInput)).
		Find(&items).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	result.Items = items
	return co.SetBody(result)
}

// @Summary	<%= database_model %> detail
// @Tags		<%= (function (){let paths = path.split('/'); return paths.map((item, index) => `${index != paths.length - 1 ? `[${item.charAt(0).toUpperCase() + item.slice(1)}]`: `${item.charAt(0).toUpperCase() + item.slice(1)}`}`).join(' ')})() %>
// @Param		id	path		int	true	"<%= database_model %> ID"
// @Success	200	{object}	common.BaseResponse{body=databases.<%= database_model %>}
// @Router		/<%= path %>/get/:id [get]
func (co <%= name %>) Get(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	var instance databases.<%= database_model %>
	if err := database.DB.First(&instance, ID).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}
	return co.SetBody(instance)
}

type <%= database_model %>CreateInput struct {
}

// @Summary	<%= database_model %> create
// @Tags		<%= (function (){let paths = path.split('/'); return paths.map((item, index) => `${index != paths.length - 1 ? `[${item.charAt(0).toUpperCase() + item.slice(1)}]`: `${item.charAt(0).toUpperCase() + item.slice(1)}`}`).join(' ')})() %>
// @Param		input	body		<%= database_model %>CreateInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/<%= path %>/create [post]
func (co <%= name %>) Create(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params <%= database_model %>CreateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	if err := database.DB.Create(&databases.<%= database_model %>{}).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

type <%= database_model %>UpdateInput struct {
}

// @Summary	<%= database_model %> edit
// @Tags		<%= (function (){let paths = path.split('/'); return paths.map((item, index) => `${index != paths.length - 1 ? `[${item.charAt(0).toUpperCase() + item.slice(1)}]`: `${item.charAt(0).toUpperCase() + item.slice(1)}`}`).join(' ')})() %>
// @Param		id		path		int					true	"<%= database_model %> ID"
// @Param		input	body		<%= database_model %>UpdateInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/<%= path %>/update/:id [put]
func (co <%= name %>) Update(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params <%= database_model %>UpdateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	ID := c.Params("id")
	if err := database.DB.Model(&databases.<%= database_model %>{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Updates(map[string]interface{}{}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

// @Summary	<%= database_model %> delete
// @Tags		<%= (function (){let paths = path.split('/'); return paths.map((item, index) => `${index != paths.length - 1 ? `[${item.charAt(0).toUpperCase() + item.slice(1)}]`: `${item.charAt(0).toUpperCase() + item.slice(1)}`}`).join(' ')})() %>
// @Param		id	path		int	true	"<%= database_model %> ID"
// @Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/<%= path %>/delete/:id [delete]
func (co <%= name %>) Delete(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	if err := database.DB.Delete(&databases.<%= database_model %>{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}