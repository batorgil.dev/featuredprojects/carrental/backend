package settings

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/integrations/database"
)

type ConfigController struct {
	common.Controller
}

func (co ConfigController) Register(router fiber.Router) {
	router.Post("set/:key", co.Set).Name("set")
	router.Get("get/:key", co.Get).Name("get")
}

// controller start

type (
	ConfigSetInput struct {
		Type  databases.ConfigType `json:"type"`
		Value string               `json:"value"`
	}
)

//	@Summary	Config pagination
//	@Tags		[Staff] [Settings] Config
//	@Param		key		path		int				true	"Config Key"
//	@Param		filter	body		ConfigSetInput	false	"Filter"
//	@Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/staff/settings/config/set/:key [post]
func (co ConfigController) Set(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	key := c.Params("key")
	if key == "" {
		return co.SetError(c, fiber.StatusBadRequest, fmt.Errorf("key must be provided"))
	}
	var params ConfigSetInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	config := databases.Config{}
	database.DB.Where("key = ?", key).First(&config)

	if config.ID != 0 {
		if err := database.DB.
			Model(&databases.Config{}).
			Where("key = ?", key).
			Updates(map[string]interface{}{
				"type":  params.Type,
				"value": params.Value,
			}).Error; err != nil {
			return co.SetError(c, fiber.StatusBadRequest, err)
		}

		return co.SetBody(common.SuccessResponse{Success: true})
	}

	if err := database.DB.Create(&databases.Config{
		Key:   key,
		Type:  params.Type,
		Value: params.Value,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

type ConfigData struct {
	Type  databases.ConfigType `json:"type"`
	Value interface{}          `json:"value"`
}

//	@Summary	Config detail
//	@Tags		[Staff] [Settings] Config
//	@Param		key	path		int	true	"Config Key"
//	@Success	200	{object}	common.BaseResponse{body=ConfigData}
//	@Router		/staff/settings/config/get/:key [get]
func (co ConfigController) Get(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	key := c.Params("key")
	var instance databases.Config
	if err := database.DB.Where("key = ?", key).First(&instance).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}
	return co.SetBody(ConfigData{
		Type:  instance.Type,
		Value: instance.GetValue(),
	})
}
