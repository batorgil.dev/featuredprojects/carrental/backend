---
to: api/controllers/<%= name %>/base.go
---
package <%= (function (){let paths = name.split('/'); return paths[paths.length -1];})() %>

import (
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/common"
	"github.com/gofiber/fiber/v2"
)

func Register(bc common.Controller, router fiber.Router) {
	// new routers
	// new controllers
}
