package databases

type (
	Location struct {
		Base
		Name string  `gorm:"name" json:"name"`
		Lat  float64 `gorm:"lat" json:"lat"`
		Long float64 `gorm:"long" json:"long"`
	}

	CarType struct {
		Base
		Name string `gorm:"column:name" json:"name"`
	}

	InsuranceCompany struct {
		Base
		Name    string `gorm:"column:name" json:"name"`
		Phone   string `gorm:"column:phone" json:"phone"`
		Website string `gorm:"column:website" json:"website"`
	}

	Contact struct {
		Base
		IconPath string `gorm:"column:icon_path" json:"icon_path"`
		Name     string `gorm:"column:name" json:"name"`
		Phone    string `gorm:"column:phone" json:"phone"`
	}

	Coupon struct {
		Base
		Code   string  `gorm:"column:code" json:"code"`
		Price  float64 `gorm:"column:price" json:"price"`
		IsUsed bool    `gorm:"column:is_used" json:"is_used"`
	}

	EmailTemplate struct {
		Base
		Name        string `gorm:"column:name" json:"name"`
		Type        string `gorm:"column:type" json:"type"`
		ContentHTML string `gorm:"column:content_html" json:"content_html"`
	}
)
