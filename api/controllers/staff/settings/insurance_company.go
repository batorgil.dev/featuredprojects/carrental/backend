package settings

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/utils"
)

type InsuranceCompanyController struct {
	common.Controller
}

func (co InsuranceCompanyController) Register(router fiber.Router) {
	router.Post("page", co.Pagination).Name("page")
	router.Post("create", co.Create).Name("create")
	router.Delete("delete/:id", co.Delete).Name("delete")
	router.Put("update/:id", co.Update).Name("update")
}

// controller start

type (
	InsuranceCompanyPageFilterInput struct {
		database.PaginationInput
		Name      *string   `json:"name"`
		Phone     *string   `json:"phone"`
		Website   *string   `json:"website"`
		CreatedAt []*string `json:"created_at"`
	}
)

//	@Summary	InsuranceCompany pagination
//	@Tags		[Staff] [Settings] Insurance_company
//	@Param		filter	body		InsuranceCompanyPageFilterInput	false	"Filter"
//	@Success	200		{object}	common.BaseResponse{body=common.PaginationResponse{items=[]databases.InsuranceCompany}}
//	@Router		/staff/settings/insurance_company/page [post]
func (co InsuranceCompanyController) Pagination(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params InsuranceCompanyPageFilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.InsuranceCompany{}),
	}

	orm = orm.
		Like("name", params.Name).
		Like("phone", params.Phone).
		Like("website", params.Website).
		BetweenDate("created_at", params.CreatedAt)

	result := common.PaginationResponse{
		Total: orm.Total(),
	}

	items := []databases.InsuranceCompany{}
	if err := orm.Scopes(database.Paginate(&params.PaginationInput)).
		Find(&items).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	result.Items = items
	return co.SetBody(result)
}

type InsuranceCompanyCreateInput struct {
	Name    string `json:"name"`
	Phone   string `json:"phone"`
	Website string `json:"website"`
}

//	@Summary	InsuranceCompany create
//	@Tags		[Staff] [Settings] Insurance_company
//	@Param		input	body		InsuranceCompanyCreateInput	true	"Input"
//	@Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/staff/settings/insurance_company/create [post]
func (co InsuranceCompanyController) Create(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params InsuranceCompanyCreateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	if err := database.DB.Create(&databases.InsuranceCompany{
		Name:    params.Name,
		Phone:   params.Phone,
		Website: params.Website,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

type InsuranceCompanyUpdateInput struct {
	Name    string `json:"name"`
	Phone   string `json:"phone"`
	Website string `json:"website"`
}

//	@Summary	InsuranceCompany edit
//	@Tags		[Staff] [Settings] Insurance_company
//	@Param		id		path		int							true	"InsuranceCompany ID"
//	@Param		input	body		InsuranceCompanyUpdateInput	true	"Input"
//	@Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/staff/settings/insurance_company/update/:id [put]
func (co InsuranceCompanyController) Update(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params InsuranceCompanyUpdateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	ID := c.Params("id")
	if err := database.DB.Model(&databases.InsuranceCompany{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Updates(map[string]interface{}{
		"name":    params.Name,
		"phone":   params.Phone,
		"website": params.Website,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

//	@Summary	InsuranceCompany delete
//	@Tags		[Staff] [Settings] Insurance_company
//	@Param		id	path		int	true	"InsuranceCompany ID"
//	@Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/staff/settings/insurance_company/delete/:id [delete]
func (co InsuranceCompanyController) Delete(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	if err := database.DB.Unscoped().Delete(&databases.InsuranceCompany{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}
