package controllers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/integrations/database"
)

type AliveController struct {
	common.Controller
}

func (co AliveController) Register(app *fiber.App) {
	app.Get("/", co.Get).Name("alive")
}

// @Summary	Alive
// @Tags		Alive
// @Success	200	{object}	common.BaseResponse
// @Router		/ [get]
func (co AliveController) Get(c *fiber.Ctx) error {
	var dbConnections int64
	if err := database.DB.Raw("select count(*) from pg_stat_activity where datname = 'dms'").Scan(&dbConnections).Error; err != nil {
		c.Status(500).JSON(fiber.Map{
			"alive":                true,
			"database_connections": false,
		})
		return nil
	}

	c.Status(200).JSON(fiber.Map{
		"alive":                true,
		"database_connections": dbConnections,
	})
	return nil
}
