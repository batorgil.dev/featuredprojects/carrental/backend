package vehicle

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/integrations/storage"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/utils"
)

type DocumentController struct {
	common.Controller
}

func (co DocumentController) Register(router fiber.Router) {
	router.Post("page", co.Pagination).Name("page")
	router.Post("create", co.Create).Name("create")
	router.Delete("delete/:id", co.Delete).Name("delete")
}

// controller start

type (
	CarDocumentPageFilterInput struct {
		database.PaginationInput
		CarID     *uint     `json:"car_id"`
		Name      *string   `json:"name"`
		CreatedAt []*string `json:"created_at"`
	}
)

// @Summary	CarDocument pagination
// @Tags		[Staff] [Vehicle] Document
// @Param		filter	body		CarDocumentPageFilterInput	false	"Filter"
// @Success	200		{object}	common.BaseResponse{body=common.PaginationResponse{items=[]databases.CarDocument}}
// @Router		/staff/vehicle/document/page [post]
func (co DocumentController) Pagination(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params CarDocumentPageFilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.CarDocument{}),
	}

	orm = orm.
		Equal("car_id", params.CarID).
		Like("name", params.Name).
		BetweenDate("created_at", params.CreatedAt)

	result := common.PaginationResponse{
		Total: orm.Total(),
	}

	items := []databases.CarDocument{}
	if err := orm.Scopes(database.Paginate(&params.PaginationInput)).
		Find(&items).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	result.Items = items
	return co.SetBody(result)
}

type CarDocumentCreateInput struct {
	CarID    uint   `json:"car_id"`
	Name     string `json:"name"`
	FilePath string `json:"file_path"`
}

// @Summary	CarDocument create
// @Tags		[Staff] [Vehicle] Document
// @Param		input	body		CarDocumentCreateInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/staff/vehicle/document/create [post]
func (co DocumentController) Create(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params CarDocumentCreateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	if err := database.DB.Create(&databases.CarDocument{
		CarID:    params.CarID,
		Name:     params.Name,
		FilePath: params.FilePath,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

// @Summary	CarDocument delete
// @Tags		[Staff] [Vehicle] Document
// @Param		id	path		int	true	"CarDocument ID"
// @Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/staff/vehicle/document/delete/:id [delete]
func (co DocumentController) Delete(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	instance := databases.CarDocument{}
	if err := database.DB.First(&instance, ID).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	if err := database.DB.Unscoped().Delete(&databases.CarDocument{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	st := storage.Connect()
	st.RemoveFile(instance.FilePath)

	return co.SetBody(common.SuccessResponse{Success: true})
}
