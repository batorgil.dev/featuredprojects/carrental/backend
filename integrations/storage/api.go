package storage

import (
	"context"
	"errors"
	"fmt"
	"mime/multipart"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/gofrs/uuid"
	"github.com/minio/minio-go/v7"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/constants"
)

func (c Storage) CreatePublicBucket(bucketName string) error {
	exists, errBucketExists := c.BucketExists(context.Background(), bucketName)
	if errBucketExists == nil && !exists {
		if errBucketCreate := c.MakeBucket(context.Background(), bucketName, minio.MakeBucketOptions{}); errBucketCreate != nil {
			return errors.New("bucket create failed: " + errBucketCreate.Error())
		}
		policy := `{"Version": "2012-10-17","Statement": [{"Action": ["s3:GetObject","s3:PutObject"],"Effect": "Allow","Principal": {"AWS": ["*"]},"Resource": ["arn:aws:s3:::` + bucketName + `/*"],"Sid": ""}]}`
		if err := c.SetBucketPolicy(context.Background(), bucketName, policy); err != nil {

			return errors.New("bucket set policy failed: " + err.Error())
		}
	}

	return nil
}

func (c Storage) RemoveFile(filePath string) error {
	splitted := strings.Split(filePath, "/")
	return c.RemoveObject(context.Background(), splitted[0], filePath[len(splitted[0]):], minio.RemoveObjectOptions{})
}

func (client Storage) UploadFromData(c *fiber.Ctx, file *multipart.FileHeader, contentType string) (string, error) {
	u2, err := uuid.NewV4()
	if err != nil {
		return "", err
	}
	if err := client.CreatePublicBucket(constants.DefaultBucketName); err != nil {
		return "", err
	}

	types := file.Header["Content-Type"]
	openFile, errorfileOpen := file.Open()
	if errorfileOpen != nil {
		return "", errorfileOpen
	}

	uploadInfo, errUploadInfo := client.PutObject(context.Background(), constants.DefaultBucketName, fmt.Sprintf("%s.%s", u2.String(), contentType), openFile, file.Size, minio.PutObjectOptions{ContentType: types[0]})
	if errUploadInfo != nil {
		return "", errUploadInfo
	}

	return uploadInfo.Bucket + "/" + uploadInfo.Key, nil
}
