package storage

import (
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"github.com/spf13/viper"
)

type Storage struct {
	*minio.Client
}

func Connect() *Storage {
	minioClient, err := minio.New(viper.GetString("STORAGE_HOST"), &minio.Options{
		Creds:  credentials.NewStaticV4(viper.GetString("STORAGE_USER"), viper.GetString("STORAGE_PASSWORD"), ""),
		Secure: false,
	})
	if err != nil {
		panic(err.Error())
	}
	return &Storage{
		Client: minioClient,
	}
}
