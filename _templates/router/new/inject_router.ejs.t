---
inject: true
to: api/controllers/<%= (function(){ let finalPath = name.split('/'); finalPath.pop(); finalPath.push('base'); return finalPath.join('/') })() %>.go
skip_if: "<%= (function (){let paths = name.split('/'); return paths[paths.length -1];})() %>.Register"
after: "new routers"
---
  <%= (function (){let paths = name.split('/'); return paths[paths.length -1];})() %>.Register(bc, router.Group("<%= (function (){let paths = name.split('/'); return paths[paths.length -1];})() %>").Name("<%= (function (){let paths = name.split('/'); return paths[paths.length -1];})() %>."))