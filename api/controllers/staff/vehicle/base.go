package vehicle

import (
  "gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/middlewares"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/common"
)

func Register(bc common.Controller, router fiber.Router) {
	// new routers
	// new controllers
  TrafficController{Controller: bc}.Register(router.Group("traffic" , middlewares.Auth).Name("traffic."))
  DamageController{Controller: bc}.Register(router.Group("damage" , middlewares.Auth).Name("damage."))
  DocumentController{Controller: bc}.Register(router.Group("document" , middlewares.Auth).Name("document."))
  NoteController{Controller: bc}.Register(router.Group("note" , middlewares.Auth).Name("note."))
  CarController{Controller: bc}.Register(router.Group("car" , middlewares.Auth).Name("car."))
}
