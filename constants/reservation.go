package constants

type ReservationStatus string

const (
	ReservationStatusNew       ReservationStatus = "new"
	ReservationStatusPaid      ReservationStatus = "paid"
	ReservationStatusConfirmed ReservationStatus = "confirmed"
	ReservationStatusPicked    ReservationStatus = "picked"
	ReservationStatusDropped   ReservationStatus = "dropped"
	ReservationStatusOverdue   ReservationStatus = "overdue"
)

type InvoiceType string

const (
	InvoiceTypeInsurance InvoiceType = "insurance"
	InvoiceTypeCustomer  InvoiceType = "customer"
)

type InvoiceStatus string

const (
	InvoiceStatusNew       InvoiceStatus = "new"
	InvoiceStatusPaid      InvoiceStatus = "pending"
	InvoiceStatusConfirmed InvoiceStatus = "paid"
)
