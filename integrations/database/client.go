package database

import (
	"fmt"

	"github.com/spf13/viper"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/databases"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var DB *gorm.DB

func LoadDatabase() {
	db, err := gorm.Open(postgres.Open(fmt.Sprintf(
		"host=%s port=%d user=%s dbname=%s password=%s sslmode=disable TimeZone=%s",
		viper.GetString("DB_HOST"),
		viper.GetInt("DB_PORT"),
		viper.GetString("DB_USER"),
		viper.GetString("DB_NAME"),
		viper.GetString("DB_PASSWORD"),
		viper.GetString("DB_TIMEZONE"),
	)), &gorm.Config{
		PrepareStmt:                              true,
		SkipDefaultTransaction:                   true,
		DisableForeignKeyConstraintWhenMigrating: true,
	})
	if err != nil {
		panic(err.Error())
	}
	db.AutoMigrate(
		// Car
		&databases.Car{},
		&databases.CarReview{},
		&databases.CarDamage{},
		&databases.CarTraffic{},
		&databases.CarNote{},
		&databases.CarDocument{},
		// Customer
		&databases.Customer{},
		&databases.CustomerFavorate{},
		&databases.CustomerNotification{},
		&databases.CustomerNote{},
		&databases.CustomerDocument{},
		&databases.EmailLog{},
		// Reservation
		&databases.Reservation{},
		&databases.Payment{},
		&databases.Invoice{},
		&databases.RerservationDocument{},
		// Settings
		&databases.Location{},
		&databases.CarType{},
		&databases.InsuranceCompany{},
		&databases.Contact{},
		&databases.Coupon{},
		&databases.EmailTemplate{},
		&databases.Config{},
		// User
		&databases.Staff{},
		&databases.StaffNotification{},
	)

	DB = db
}
