package constants

const (
	DefaultBucketName string = "files"
)

// UserRole types
type UserRole string

const (
	UserRoleAdmin UserRole = "admin"
	UserRoleUser  UserRole = "user"
)

type PaymentType string

const (
	PaymentTypeQpay      PaymentType = "qpay"
	PaymentTypeSocialPay PaymentType = "social_pay"
)
