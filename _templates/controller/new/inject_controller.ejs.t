---
inject: true
to: api/controllers/<%= (function(){ let finalPath = path.split('/'); finalPath.pop(); finalPath.push('base'); return finalPath.join('/') })() %>.go
skip_if: <%= name %>
after: "new controllers"
---
  <%= name %>{Controller: bc}.Register(router.Group("<%= (function () {let paths = path.split('/'); return paths[paths.length -1]})() %>" <% if (middleware){%>, middlewares.<%= middleware %><%}%>).Name("<%= (function () {let paths = path.split('/'); return paths[paths.length -1]})() %>."))