package mail

type (
	EmailType string

	EmailInput struct {
		Email    string    `json:"email"`
		Subtitle string    `json:"subtitle"`
		Type     EmailType `json:"type"`
	}

	VerifyInput struct {
		Code string `json:"code"`
		Url  string `json:"url"`
		Year string `json:"year"`
	}

	ForgotPasswordInput struct {
		Code string `json:"code"`
		Url  string `json:"url"`
		Year string `json:"year"`
	}

	EmailDirectInput struct {
		Title string
		Body  string
	}
)

const (
	EmailTypeDirect EmailType = "emails-direct"

	EmailTypeVerify            EmailType = "emails-verify"
	EmailTypeVerifyNew         EmailType = "emails-verify-new"
	EmailTypeForgotPassword    EmailType = "emails-forgot-password"
	EmailTypeForgotPasswordNew EmailType = "emails-forgot-password-new"
)
