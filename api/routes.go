package api

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/gofiber/swagger"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/common"
	_ "gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/docs"
)

func Register(app *fiber.App) *fiber.App {
	bc := common.Controller{
		Res: &common.BaseResponse{
			Message:    "",
			Body:       nil,
			StatusCode: 200,
		},
	}
	// Global middlewares
	app.Use(cors.New())
	app.Use(recover.New())

	// Static file server
	app.Static("static", "./files/public")

	// Swagger init
	app.Get("/swagger/*", swagger.HandlerDefault)
	controllers.AliveController{Controller: bc}.Register(app)

	// Register controllers
	controllers.Register(bc, app.Group("api/v1"))
	return app
}
