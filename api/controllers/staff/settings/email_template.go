package settings

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/utils"
)

type EmailTemplateController struct {
	common.Controller
}

func (co EmailTemplateController) Register(router fiber.Router) {
	router.Post("page", co.Pagination).Name("page")
	router.Post("create", co.Create).Name("create")
	router.Delete("delete/:id", co.Delete).Name("delete")
	router.Put("update/:id", co.Update).Name("update")
}

// controller start

type (
	EmailTemplatePageFilterInput struct {
		database.PaginationInput
		Name      *string   `json:"name"`
		Type      *string   `json:"type"`
		CreatedAt []*string `json:"created_at"`
	}
)

//	@Summary	EmailTemplate pagination
//	@Tags		[Staff] [Settings] Email_template
//	@Param		filter	body		EmailTemplatePageFilterInput	false	"Filter"
//	@Success	200		{object}	common.BaseResponse{body=common.PaginationResponse{items=[]databases.EmailTemplate}}
//	@Router		/staff/settings/email_template/page [post]
func (co EmailTemplateController) Pagination(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params EmailTemplatePageFilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.EmailTemplate{}),
	}

	orm = orm.
		Like("name", params.Name).
		Equal("type", params.Type).
		BetweenDate("created_at", params.CreatedAt)

	result := common.PaginationResponse{
		Total: orm.Total(),
	}

	items := []databases.EmailTemplate{}
	if err := orm.Scopes(database.Paginate(&params.PaginationInput)).
		Find(&items).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	result.Items = items
	return co.SetBody(result)
}

type EmailTemplateCreateInput struct {
	Name        string `json:"name"`
	Type        string `json:"type"`
	ContentHTML string `json:"content_html"`
}

//	@Summary	EmailTemplate create
//	@Tags		[Staff] [Settings] Email_template
//	@Param		input	body		EmailTemplateCreateInput	true	"Input"
//	@Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/staff/settings/email_template/create [post]
func (co EmailTemplateController) Create(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params EmailTemplateCreateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	if err := database.DB.Where("type = ?", params.Type).First(&databases.EmailTemplate{}).Error; err == nil {
		return co.SetError(c, fiber.StatusBadRequest, fmt.Errorf("email type already exists"))
	}

	if err := database.DB.Create(&databases.EmailTemplate{
		Name:        params.Name,
		Type:        params.Type,
		ContentHTML: params.ContentHTML,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

type EmailTemplateUpdateInput struct {
	Name        string `json:"name"`
	Type        string `json:"type"`
	ContentHTML string `json:"content_html"`
}

//	@Summary	EmailTemplate edit
//	@Tags		[Staff] [Settings] Email_template
//	@Param		id		path		int							true	"EmailTemplate ID"
//	@Param		input	body		EmailTemplateUpdateInput	true	"Input"
//	@Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/staff/settings/email_template/update/:id [put]
func (co EmailTemplateController) Update(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params EmailTemplateUpdateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	ID := c.Params("id")
	if err := database.DB.Model(&databases.EmailTemplate{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Updates(map[string]interface{}{
		"name":         params.Name,
		"type":         params.Type,
		"content_html": params.ContentHTML,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

//	@Summary	EmailTemplate delete
//	@Tags		[Staff] [Settings] Email_template
//	@Param		id	path		int	true	"EmailTemplate ID"
//	@Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
//	@Router		/staff/settings/email_template/delete/:id [delete]
func (co EmailTemplateController) Delete(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	if err := database.DB.Unscoped().Delete(&databases.EmailTemplate{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}
