package vehicle

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/utils"
)

type NoteController struct {
	common.Controller
}

func (co NoteController) Register(router fiber.Router) {
	router.Post("page", co.Pagination).Name("page")
	router.Post("create", co.Create).Name("create")
	router.Delete("delete/:id", co.Delete).Name("delete")
	router.Put("update/:id", co.Update).Name("update")
}

// controller start

type (
	CarNotePageFilterInput struct {
		database.PaginationInput
		CarID     *uint     `json:"car_id"`
		Note      *string   `json:"note"`
		CreatedAt []*string `json:"created_at"`
	}
)

// @Summary	CarNote pagination
// @Tags		[Staff] [Vehicle] Note
// @Param		filter	body		CarNotePageFilterInput	false	"Filter"
// @Success	200		{object}	common.BaseResponse{body=common.PaginationResponse{items=[]databases.CarNote}}
// @Router		/staff/vehicle/note/page [post]
func (co NoteController) Pagination(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params CarNotePageFilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.CarNote{}),
	}

	orm = orm.
		Equal("car_id", params.CarID).
		Like("note", params.Note).
		BetweenDate("created_at", params.CreatedAt)

	result := common.PaginationResponse{
		Total: orm.Total(),
	}

	items := []databases.CarNote{}
	if err := orm.Scopes(database.Paginate(&params.PaginationInput)).
		Find(&items).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	result.Items = items
	return co.SetBody(result)
}

type CarNoteCreateInput struct {
	CarID uint   `json:"car_id"`
	Note  string `json:"note"`
}

// @Summary	CarNote create
// @Tags		[Staff] [Vehicle] Note
// @Param		input	body		CarNoteCreateInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/staff/vehicle/note/create [post]
func (co NoteController) Create(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params CarNoteCreateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	if err := database.DB.Create(&databases.CarNote{
		CarID: params.CarID,
		Note:  params.Note,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

type CarNoteUpdateInput struct {
	CarID uint   `json:"car_id"`
	Note  string `json:"note"`
}

// @Summary	CarNote edit
// @Tags		[Staff] [Vehicle] Note
// @Param		id		path		int					true	"CarNote ID"
// @Param		input	body		CarNoteUpdateInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/staff/vehicle/note/update/:id [put]
func (co NoteController) Update(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params CarNoteUpdateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	ID := c.Params("id")
	if err := database.DB.Model(&databases.CarNote{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Updates(map[string]interface{}{
		"car_id": params.CarID,
		"note":   params.Note,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

// @Summary	CarNote delete
// @Tags		[Staff] [Vehicle] Note
// @Param		id	path		int	true	"CarNote ID"
// @Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/staff/vehicle/note/delete/:id [delete]
func (co NoteController) Delete(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	if err := database.DB.Unscoped().Delete(&databases.CarNote{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}
