package common

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/utils"
	"go.uber.org/zap"
)

type Controller struct {
	Res *BaseResponse
}

func (co Controller) RespondPanic(c *fiber.Ctx, err interface{}) error {
	utils.Error("PANIC", zap.String("", fmt.Sprint(err)))
	return c.Status(500).JSON(&fiber.Map{
		"body":    nil,
		"message": "panic error happend",
	})
}

func (co Controller) GetBody(c *fiber.Ctx) {
	if co.Res.Message != "" {
		return
	}
	c.Status(200).JSON(co.Res)
}

func (co Controller) SetBody(body interface{}) error {
	co.Res.StatusCode = 200
	co.Res.Message = ""
	co.Res.Body = body
	return nil
}

func (co Controller) SetError(c *fiber.Ctx, code int, err error) error {
	co.Res.StatusCode = 500
	co.Res.Message = err.Error()
	if err != nil {
		utils.Error(c.BaseURL(), zap.String("Error", err.Error()))
	}
	if code == 0 {
		co.Res.StatusCode = fiber.StatusInternalServerError
	} else {
		co.Res.StatusCode = code
	}
	co.Res.Body = nil
	return c.Status(co.Res.StatusCode).JSON(co.Res)
}

func (co Controller) GetConfig(key string) (databases.Config, error) {
	var config databases.Config
	if err := database.DB.Where("key = ?", key).First(&config).Error; err != nil {
		return config, err
	}
	return config, nil
}
