package databases

import (
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/constants"
)

type (
	Staff struct {
		Base
		FirstName  string             `gorm:"column:first_name;not null" json:"first_name"`
		LastName   string             `gorm:"column:last_name;not null" json:"last_name"`
		AvatarPath string             `gorm:"column:avatar_path" json:"avatar_path"`
		Email      string             `gorm:"index:email,unique;not null" json:"email"`
		Position   string             `gorm:"column:position" json:"position"`
		Bio        string             `gorm:"column:bio" json:"bio"`
		Password   string             `gorm:"column:password" json:"-"`
		IsActive   bool               `gorm:"column:is_active" json:"is_active"`
		Role       constants.UserRole `gorm:"column:role" json:"role"`
	}

	StaffNotification struct {
		Base
		StaffID       uint   `gorm:"column:staff_id" json:"staff_id"`
		IsSeen        bool   `gorm:"column:is_seen" json:"is_seen"`
		Message       string `gorm:"column:message" json:"message"`
		ReservationID uint   `gorm:"column:reservetion_id" json:"reservetion_id"`
	}
)
