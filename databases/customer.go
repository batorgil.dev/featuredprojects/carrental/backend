package databases

import "time"

type (
	Customer struct {
		Base
		FirstName        string    `gorm:"column:first_name" json:"first_name"`
		LastName         string    `gorm:"column:last_name" json:"last_name"`
		Phone            string    `gorm:"column:phone" json:"phone"`
		Email            string    `gorm:"column:email" json:"email"`
		AvatarPath       string    `gorm:"column:avatar_path" json:"avatar_path"`
		Address          string    `gorm:"column:address" json:"address"`
		LisenceNo        string    `gorm:"column:lisence_no" json:"lisence_no"`
		LisenceExpiredAt time.Time `gorm:"column:lisence_expired_at" json:"lisence_expired_at"`
		LisenceFrontPath string    `gorm:"column:lisence_front_path" json:"lisence_front_path"`
		LisenceBackPath  string    `gorm:"column:lisence_back_path" json:"lisence_back_path"`
		Password         string    `gorm:"column:password" json:"-"`
	}

	CustomerFavorate struct {
		Base
		CarID      uint `gorm:"column:car_id" json:"car_id"`
		CustomerID uint `gorm:"column:customer_id" json:"customer_id"`
	}

	CustomerNotification struct {
		Base
		CustomerID    uint   `gorm:"column:customer_id" json:"customer_id"`
		IsSeen        bool   `gorm:"column:is_seen" json:"is_seen"`
		Message       string `gorm:"column:message" json:"message"`
		ReservationID uint   `gorm:"column:reservetion_id" json:"reservetion_id"`
	}

	CustomerNote struct {
		Base
		CustomerID uint   `gorm:"column:customer_id" json:"customer_id"`
		Note       string `gorm:"column:note" json:"note"`
	}

	CustomerDocument struct {
		Base
		CustomerID uint   `gorm:"column:customer_id" json:"customer_id"`
		Name       string `gorm:"column:name" json:"name"`
		FilePath   string `gorm:"column:file_path" json:"file_path"`
	}

	EmailLog struct {
		Base
		CustomerID    uint   `gorm:"column:customer_id" json:"customer_id"`
		ReservationID uint   `gorm:"column:reservation_id" json:"reservation_id"`
		Type          string `gorm:"column:type" json:"type"` // customer or from mail template type
	}
)
