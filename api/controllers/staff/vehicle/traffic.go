package vehicle

import (
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/databases"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/integrations/database"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/utils"
)

type TrafficController struct {
	common.Controller
}

func (co TrafficController) Register(router fiber.Router) {
	router.Post("page", co.Pagination).Name("page")
	router.Post("create", co.Create).Name("create")
	router.Delete("delete/:id", co.Delete).Name("delete")
	router.Put("update/:id", co.Update).Name("update")
}

// controller start

type (
	CarTrafficPageFilterInput struct {
		database.PaginationInput
		Date          *time.Time `json:"date"`
		Note          *string    `json:"note"`
		IsPaid        *string    `json:"is_paid"`
		TicketNo      *string    `json:"ticket_no"`
		CarID         *uint      `json:"car_id"`
		ReservationID *uint      `json:"reservation_id"`
		CustomerID    *uint      `json:"customer_id"`
		CreatedAt     []*string  `json:"created_at"`
	}
)

// @Summary	CarTraffic pagination
// @Tags		[Staff] [Vehicle] Traffic
// @Param		filter	body		CarTrafficPageFilterInput	false	"Filter"
// @Success	200		{object}	common.BaseResponse{body=common.PaginationResponse{items=[]databases.CarTraffic}}
// @Router		/staff/vehicle/traffic/page [post]
func (co TrafficController) Pagination(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params CarTrafficPageFilterInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	orm := &database.QueryBuilder{
		DB: database.DB.Model(&databases.CarTraffic{}),
	}

	orm = orm.
		Like("note", params.Note).
		Like("ticket_no", params.TicketNo).
		Equal("car_id", params.CarID).
		Equal("customer_id", params.CustomerID).
		Equal("reservation_id", params.ReservationID).
		Bool("is_paid", params.IsPaid).
		BetweenDate("date", params.Date).
		BetweenDate("created_at", params.CreatedAt)

	result := common.PaginationResponse{
		Total: orm.Total(),
	}

	items := []databases.CarTraffic{}
	if err := orm.Scopes(database.Paginate(&params.PaginationInput)).
		Find(&items).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}
	result.Items = items
	return co.SetBody(result)
}

type CarTrafficCreateInput struct {
	Date          time.Time `json:"date"`
	Note          string    `json:"note"`
	IsPaid        bool      `json:"is_paid"`
	TicketNo      string    `json:"ticket_no"`
	Amount        float64   `json:"amount"`
	CarID         uint      `json:"car_id"`
	ReservationID uint      `json:"reservation_id"`
	CustomerID    uint      `json:"customer_id"`
}

// @Summary	CarTraffic create
// @Tags		[Staff] [Vehicle] Traffic
// @Param		input	body		CarTrafficCreateInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/staff/vehicle/traffic/create [post]
func (co TrafficController) Create(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params CarTrafficCreateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	if err := database.DB.Create(&databases.CarTraffic{
		Date:          params.Date,
		Note:          params.Note,
		IsPaid:        params.IsPaid,
		TicketNo:      params.TicketNo,
		Amount:        params.Amount,
		CarID:         params.CarID,
		ReservationID: params.ReservationID,
		CustomerID:    params.CustomerID,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

type CarTrafficUpdateInput struct {
	Date          time.Time `json:"date"`
	Note          string    `json:"note"`
	Amount        float64   `json:"amount"`
	IsPaid        bool      `json:"is_paid"`
	TicketNo      string    `json:"ticket_no"`
	CarID         uint      `json:"car_id"`
	ReservationID uint      `json:"reservation_id"`
	CustomerID    uint      `json:"customer_id"`
}

// @Summary	CarTraffic edit
// @Tags		[Staff] [Vehicle] Traffic
// @Param		id		path		int						true	"CarTraffic ID"
// @Param		input	body		CarTrafficUpdateInput	true	"Input"
// @Success	200		{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/staff/vehicle/traffic/update/:id [put]
func (co TrafficController) Update(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	var params CarTrafficUpdateInput
	if err := c.BodyParser(&params); err != nil {
		return co.SetError(c, fiber.StatusBadRequest, err)
	}

	ID := c.Params("id")
	if err := database.DB.Model(&databases.CarTraffic{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Updates(map[string]interface{}{
		"date":           params.Date,
		"note":           params.Note,
		"is_paid":        params.IsPaid,
		"ticket_no":      params.TicketNo,
		"car_id":         params.CarID,
		"amount":         params.Amount,
		"reservation_id": params.ReservationID,
		"customer_id":    params.CustomerID,
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}

// @Summary	CarTraffic delete
// @Tags		[Staff] [Vehicle] Traffic
// @Param		id	path		int	true	"CarTraffic ID"
// @Success	200	{object}	common.BaseResponse{body=common.SuccessResponse}
// @Router		/staff/vehicle/traffic/delete/:id [delete]
func (co TrafficController) Delete(c *fiber.Ctx) error {
	defer func() {
		if r := recover(); r != nil {
			co.RespondPanic(c, r)
		} else {
			co.GetBody(c)
		}
	}()

	ID := c.Params("id")
	if err := database.DB.Unscoped().Delete(&databases.CarTraffic{
		Base: databases.Base{ID: utils.Str2Uint(ID)},
	}).Error; err != nil {
		return co.SetError(c, fiber.StatusInternalServerError, err)
	}

	return co.SetBody(common.SuccessResponse{Success: true})
}
