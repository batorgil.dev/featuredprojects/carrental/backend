package mail

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"html/template"
	"log"
	"net/smtp"

	"github.com/spf13/viper"
)

type Client struct {
	From     string
	Password string
	SmtpHost string
	SmtpPort string
}

func Connect() *Client {
	return &Client{
		From:     viper.GetString("NOREPLY_EMAIL"),
		Password: viper.GetString("NOREPLY_EMAIL_PASSWORD"),
		SmtpHost: "smtp.zoho.com",
		SmtpPort: "465",
	}
}

func (c Client) Send(input EmailInput, params interface{}) error {
	t, errTemp := template.New("").ParseFiles(fmt.Sprintf("files/email/%s.html", input.Type), "files/email/base.html")
	if errTemp != nil {
		fmt.Println("PARSING TEMPLATE ERROR : ", errTemp.Error())
		return errTemp
	}
	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         c.SmtpHost,
	}

	auth := smtp.PlainAuth(c.From, c.From, c.Password, c.SmtpHost)
	conn, err := tls.Dial("tcp", c.SmtpHost+":"+c.SmtpPort, tlsconfig)
	if err != nil {
		log.Panic(err)
	}

	client, err := smtp.NewClient(conn, c.SmtpHost)
	if err != nil {
		log.Panic(err)
	}

	if err = client.Auth(auth); err != nil {
		log.Panic(err)
	}

	if err = client.Mail(c.From); err != nil {
		log.Panic(err)
	}

	if err = client.Rcpt(input.Email); err != nil {
		log.Panic(err)
	}

	w, err := client.Data()
	if err != nil {
		log.Panic(err)
	}

	// err = t.ExecuteTemplate(w, "base", params)
	// if err != nil {
	// 	fmt.Println(err)
	// }
	var body bytes.Buffer

	mimeHeaders := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	body.Write([]byte(fmt.Sprintf("From: %s\r\nTo: %s\r\nSubject: %s \n%s\n\n", c.From, input.Email, input.Subtitle, mimeHeaders)))

	t.ExecuteTemplate(&body, "base", params)
	_, err = w.Write(body.Bytes())
	if err != nil {
		log.Panic(err)
	}
	// Sending email.
	err = w.Close()
	if err != nil {
		log.Panic(err)
	}

	client.Quit()

	log.Println("Mail sent successfully")
	return nil
}
