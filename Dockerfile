#Stage 1: BUILD
FROM golang:alpine AS BUILD
ENV GO111MODULE=on
RUN apk add bash git gcc g++ libc-dev make
WORKDIR /go/src/gitlab.com/batorgil.dev/featuredprojects/carrental/backend
COPY . .
RUN go mod tidy
RUN CGO_ENABLED=0 GOOS=linux go build -a -gcflags='-N -l' -installsuffix cgo -o main main.go

# Stage 2: RUN
FROM alpine:3.13
RUN apk update && apk add tzdata ca-certificates
ENV TZ Asia/Ulaanbaatar
WORKDIR /home
COPY --from=BUILD /go/src/gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/docs /home/api/docs
COPY --from=BUILD /go/src/gitlab.com/batorgil.dev/featuredprojects/carrental/backend/files /home/files
COPY --from=BUILD /go/src/gitlab.com/batorgil.dev/featuredprojects/carrental/backend/main /home/
COPY --from=BUILD /go/src/gitlab.com/batorgil.dev/featuredprojects/carrental/backend/env_prod /home/.env

EXPOSE 8000
ENTRYPOINT ["/home/main"]