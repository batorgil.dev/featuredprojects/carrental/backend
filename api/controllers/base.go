package controllers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/staff"
)

func Register(bc common.Controller, router fiber.Router) {
	// new routers
	staff.Register(bc, router.Group("staff").Name("staff."))
	// new controllers
	FileController{Controller: bc}.Register(router.Group("file").Name("file."))
}
