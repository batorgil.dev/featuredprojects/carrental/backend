package databases

import (
	"time"

	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/constants"
)

type (
	Reservation struct {
		Base
		ReservationNo     string                      `gorm:"column:reservation_no" json:"reservation_no"`
		FullName          string                      `gorm:"column:full_name" json:"full_name"`
		Phone             string                      `gorm:"column:phone" json:"phone"`
		Email             string                      `gorm:"column:email" json:"email"`
		Address           string                      `gorm:"column:address" json:"address"`
		PickUp            time.Time                   `gorm:"column:pick_up" json:"pick_up"`
		DropOff           time.Time                   `gorm:"column:drop_off" json:"drop_off"`
		Status            constants.ReservationStatus `gorm:"column:status" json:"status"`
		PickUpLocationID  uint                        `gorm:"column:pick_up_location_id" json:"pick_up_location_id"`
		PickUpLocation    *Location                   `gorm:"foreignKey:PickUpLocationID" json:"pick_up_location,omitempty"`
		DropOffLocationID uint                        `gorm:"column:drop_off_location_id" json:"drop_off_location_id"`
		DropOffLocation   *Location                   `gorm:"foreignKey:DropOffLocationID" json:"drop_off_location,omitempty"`
		CustomerID        uint                        `gorm:"column:customer_id" json:"customer_id"`
		Customer          *Customer                   `gorm:"foreignKey:CustomerID" json:"customer,omitempty"`
		CarID             uint                        `gorm:"column:car_id" json:"car_id"`
		Car               *Car                        `gorm:"foreignKey:CarID" json:"car,omitempty"`
	}

	Payment struct {
		Base
		TotalPrice    float64               `gorm:"column:total_price" json:"total_price"`
		CouponID      uint                  `gorm:"column:coupon_id" json:"coupon_id"`
		Coupon        *Coupon               `gorm:"foreignKey:CouponID" json:"coupon,omitempty"`
		IsPaid        bool                  `gorm:"column:is_paid" json:"is_paid"` // Төлбөр төлсөн эсэх
		UUID          string                `gorm:"column:uuid" json:"uuid"`
		PaymentType   constants.PaymentType `gorm:"column:payment_type" json:"payment_type"`
		OrgRegNo      string                `gorm:"column:org_reg_no" json:"org_reg_no"`
		OrgName       string                `gorm:"column:org_name" json:"org_name"`
		BankInvoice   string                `gorm:"column:bank_invoice" json:"bank_invoice"`
		BankQrCode    string                `gorm:"column:bank_qr_code" json:"bank_qr_code"`
		ReservationID uint                  `gorm:"column:reservation_id" json:"reservation_id"`
		InvoiceID     uint                  `gorm:"column:invoice_id" json:"invoice_id"`
	}

	Invoice struct {
		Base
		Note          string                  `gorm:"column:note" json:"note"`
		Status        constants.InvoiceStatus `gorm:"column:status" json:"status"`
		TotalPrice    float64                 `gorm:"column:total_price" json:"total_price"`
		Type          constants.InvoiceType   `gorm:"column:type" json:"type"`
		ReservationID uint                    `gorm:"column:reservation_id" json:"reservation_id"`
		CustomerID    uint                    `gorm:"column:customer_id" json:"customer_id"`
		InsuranceID   uint                    `gorm:"column:insurance_id" json:"insurance_id"`
	}

	RerservationDocument struct {
		Base
		ReservationID uint   `gorm:"column:reservation_id" json:"reservation_id"`
		Name          string `gorm:"column:name" json:"name"`
		FilePath      string `gorm:"column:file_path" json:"file_path"`
	}
)
