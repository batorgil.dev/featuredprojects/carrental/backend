package staff

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/common"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/staff/customer"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/staff/dashboard"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/staff/planner"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/staff/reservation"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/staff/settings"
	"gitlab.com/batorgil.dev/featuredprojects/carrental/backend/api/controllers/staff/vehicle"
)

func Register(bc common.Controller, router fiber.Router) {
	AuthController{Controller: bc}.Register(router.Group("auth"))

	// new routers
	vehicle.Register(bc, router.Group("vehicle").Name("vehicle."))
	reservation.Register(bc, router.Group("reservation").Name("reservation."))
	customer.Register(bc, router.Group("customer").Name("customer."))
	planner.Register(bc, router.Group("planner").Name("planner."))
	dashboard.Register(bc, router.Group("dashboard").Name("dashboard."))
	settings.Register(bc, router.Group("settings").Name("settings."))
	// new controllers
}
